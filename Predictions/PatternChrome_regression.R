# 
#   Copyright (C) 2024  Niels Benjamin Paul
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

######################################################
# Author: Jonas Wolber, Niels Benjamin Paul

# Date of last change: 27.02.2024

# Description: This is a version of PatternChrome with
# the aim of using regression for gene expression
# prediction
#
######################################################

#### Load libraries ####
library(parallel)
library(xgboost)
library(caTools)
library(pROC)
library(pso)
library(dplyr)
library(yaml)

#### Paths ####
Paths <- yaml::read_yaml("../Path_config.yaml")
setwd(Paths$PatternChrome_dir)

#### Parameters ####
# Metaparameters
options(warn=-1) # turn off error messages
report <- FALSE # whether to report progress during training
report_frequency <- 5 # report frequency during PSO optimization
rounding_decimal <- 5 # roundig decimal
k_fold <- 10 # Number of k-fold crossvalidation checks
date = gsub("-","_",as.character(Sys.Date())) # Date will be added to stored data name

target_mse <- 1

# Training parameters
num_cps <- 7 # maximal number of anchor points of pattern
bin_size <- 50 # bin size, do not change this parameter as binning was done beforehand
max_train_accuracy <- 0.999 # training accuracy where feature extraction step stops
num_sample_genes <- 3000 # number of sample genes per objective function call
min_distance <- (num_cps*3)^2 # minimum length of search area for patterns
max_distance <- max_distance <- (floor(10000/bin_size/3))^2 # maximum length of search area for patterns
objective_lower <- c(1,1,1.0001,0.25,floor(num_cps/2),rep(0,num_cps)) # lower bounds of parameters in PSO for feature extraction
objective_upper <- c(floor(10000/bin_size),floor(10000/bin_size),5.999,0.75,num_cps,rep(1,num_cps)) # upper bounds of parameters in PSO for feature extraction

# PSO parameters during feature extraction and hyperparameter tuning
# read about PSO parameters here: https://www.rdocumentation.org/packages/pso/versions/1.0.4/topics/psoptim
initial_swarm_size <- 20 # parameters starting with initial are increased by 1 when a PSO round does not yield an improved AUC score
initial_maxit_stagnate <- 3
initial_maxit <- 20
exploitation_values <- c(0.8, 0.4)
c_p <- 2.05
c_g <- 2.05

# XGBoost parameters during training and backward elimination
# read about XGBoost parameters here: https://xgboost.readthedocs.io/en/stable/parameter.html
xgb_early_stopping_rounds <- 10
nrounds <- 50
eta <- 0.2

# Hyperparameter tuning parameters
hyperparameter_lower <- c(300, 0.005, 0, 1, 1, 1, 0, 0.1) # lower bounds of parameters in PSO for hyperparameter tuning
hyperparameter_upper <- c(700, 0.2, 10, 10, 5, 10, 10, 0.7) # upper bounds of parameters in PSO for hyperparameter tuning
hp_swarm_size <- 20
hp_maxit_stagnate <- 3
hp_maxit <- 20


#### Objective function ####
Objective <- function(pars){
  if(!between((floor(pars[1])-floor(pars[2]))^2, min_distance, max_distance)){return(Inf)}
  switch (floor(pars[3]),
          hm_train <- H3K4me3_train[,floor(pars[1]):floor(pars[2])],
          hm_train <- H3K4me1_train[,floor(pars[1]):floor(pars[2])],
          hm_train <- H3K36me3_train[,floor(pars[1]):floor(pars[2])],
          hm_train <- H3K27me3_train[,floor(pars[1]):floor(pars[2])],
          hm_train <- H3K9me3_train[,floor(pars[1]):floor(pars[2])]
  ) 
  train_genes <- sample(train_genes, num_sample_genes)
  mp <- pars[6:(5+floor(pars[5]))]
  checked_positions <- 1:(ncol(hm_train)-floor(pars[5])+1)# Niels +1
  optim_df <- cbind(train_df[train_genes,],parSapply(cl,train_genes, function(g){
    sum(sapply(checked_positions, function(pos){
      cor(mp, hm_train[g,pos:(pos+length(mp)-1)])>pars[4]
    }), na.rm = T)
  }))
  return(mean((predict(xgb.train(
    data = xgb.DMatrix(data = as.matrix(optim_df[,-1]), label = optim_df[,1]),
    nrounds = nrounds, eta = eta, verbose = 0, objective = "reg:squarederror", nthread = Paths$n_workers,
    tree_method = "hist"),
    xgb.DMatrix(data = as.matrix(optim_df[,-1]), label = optim_df[,1])) - optim_df[,1])^2))
}

#### Hyperparametertuning function ####
hyperparameter_tuning <- function(hyperparams,Dataset,fn_train){# xgb_validation substituted by Dataset
  # round(auc(response = getinfo(Dataset, name = "label"), 
  #   predictor = predict(xgb.train(data = fn_train, verbose = 0,  nthread = Paths$n_workers, 
  #   nrounds = hyperparams[1], eta = hyperparams[2], gamma = hyperparams[3], max_depth = round(hyperparams[4]), 
  #   lambda = hyperparams[5], alpha = hyperparams[6], min_child_weight = hyperparams[7], subsample = hyperparams[8], 
  #   objective = "reg:squarederror", eval.metric = "auc",tree_method = "hist")
  #   , Dataset), quiet = T),rounding_decimal)
  
  mean((predict(xgb.train(data = fn_train, verbose = 0,  nthread = Paths$n_workers, 
                          nrounds = hyperparams[1], eta = hyperparams[2], gamma = hyperparams[3], max_depth = round(hyperparams[4]), 
                          lambda = hyperparams[5], alpha = hyperparams[6], min_child_weight = hyperparams[7], subsample = hyperparams[8], 
                          objective = "reg:squarederror",tree_method = "hist"),
                Dataset) - getinfo(Dataset,name = "label"))^2)
}


# fn = hyperparameter_tuning,Dataset = xgb_validation,fn_train = xgb_train

#### Sample splitting function ####
# Function that splits the Set in N_splits+1 Groups while preserving the ratio of labels
# uses the sample.split() function from the caTools package
# returns a Vector of Group assignments
# Sample_Split_mult <- function(Label,N_splits = 3){
#   Block = rep(0,length(Label))
#   for(i in N_splits:1){
#     # define set to be splited
#     Max_val <- max(Block)
#     # calculate split
#     set.seed(42)
#     To_true <- sample.split(Label[Block == Max_val],SplitRatio = (1-1/(i+1)))
#     # add split
#     Block[Block == Max_val][which(To_true)] <- Max_val + 1
#   }
#   return(Block)
# }

#### Main for loop ####
load(Paths$RNAseq_path)
setwd(Paths$main_path)
cell_lines <-  list.dirs(full.names = F, path = Paths$main_path)[-1]
stats <- c()
for (cell_line in cell_lines) {
  print(cell_line)
  t1 <- Sys.time()
  #### Load CHIP_Seq data ####
  setwd(paste(Paths$main_path, cell_line, sep = ""))
  load(paste("H3K4me3_",bin_size, "bp_bins.RData", sep =""))
  load(paste("H3K4me1_",bin_size, "bp_bins.RData", sep =""))
  load(paste("H3K36me3_",bin_size, "bp_bins.RData", sep =""))
  load(paste("H3K27me3_",bin_size, "bp_bins.RData", sep =""))
  load(paste("H3K9me3_",bin_size, "bp_bins.RData", sep =""))
  
  #### Load RNA_Seq data and binarize and sort it ####
  RNA <- as.numeric(RNA_seq[rownames(H3K27me3),cell_line])
  RNA <- log(RNA + 0.01)
  names(RNA) <- rownames(H3K27me3)
  
  #### Train, validation and test subsets ####
  set.seed(42)
  train_genes <- names(RNA)[sample.split(RNA, SplitRatio = 6601)]
  #train_genes <- sample(names(RNA),6601,replace = FALSE)
  validation_test_genes <- names(RNA)[!names(RNA) %in% train_genes]
  H3K4me3_train <- H3K4me3[train_genes,]
  H3K4me1_train <- H3K4me1[train_genes,]
  H3K36me3_train <- H3K36me3[train_genes,]
  H3K27me3_train <- H3K27me3[train_genes,]
  H3K9me3_train <- H3K9me3[train_genes,]
  H3K4me3_validation_test <- H3K4me3[validation_test_genes,]
  H3K4me1_validation_test <- H3K4me1[validation_test_genes,]
  H3K36me3_validation_test <- H3K36me3[validation_test_genes,]
  H3K27me3_validation_test <- H3K27me3[validation_test_genes,]
  H3K9me3_validation_test <- H3K9me3[validation_test_genes,]
  
  train_df <- data.frame(GE = RNA[train_genes])
  validation_test_df <- data.frame(GE = RNA[validation_test_genes])
  
  rm(H3K4me3,H3K4me1,H3K36me3,H3K27me3,H3K9me3) # remove redundant data
  
  #### Initialisation ####
  improvement_threshold <- 50
  no_improvement_round <- 0
  maxit <-initial_maxit
  swarm_size <- initial_swarm_size
  maxit_stagnate <- initial_maxit_stagnate
  improving <- TRUE
  patterns <- data.frame(Pattern_name = 0,Cell_line = 0, Start = 0, End = 0, HM = 0, MP_threshold = 0, Width = 0)
  for (p in seq_len(num_cps)) {
    patterns <- cbind.data.frame(patterns, Pattern = 0)
    colnames(patterns)[length(patterns)] <- paste("Point", p, sep = "_")
  }
  cl <- makePSOCKcluster(Paths$n_workers)
  
  Random_seed <- 42
  
  parallel::setDefaultCluster(cl)
  # Do something in each cluster
  (parRes <- do.call(c, parallel::clusterApply(x = 1:Paths$n_workers+Random_seed-1, fun = function(x) {
    set.seed(x)
    #runif(1)
  })))
  
  
  
  Pattern_index = 1
  
  
  while(improving){
    
    #### Optimization function ####
    set.seed(Random_seed)
    Random_seed <- Random_seed + 1
    
    
    res <- psoptim(par = rep(NA, length(objective_upper)), fn = Objective, 
                   lower = objective_lower, upper = objective_upper, 
                   control = list(
                     vectorize = T, abstol = 0.1, 
                     s = swarm_size, w = exploitation_values, maxit = maxit, c.p = c_p, c.g = c_g, 
                     trace = report, REPORT = report_frequency,
                     maxit.stagnate = maxit_stagnate))
    if(res$value < improvement_threshold){
      patterns <- rbind(patterns, c(paste0("Pattern_",Pattern_index),cell_line, res$par))
      Pattern_index = Pattern_index + 1
      switch (res$par[3],
              hm_train <- H3K4me3_train[,res$par[1]:res$par[2]],
              hm_train <- H3K4me1_train[,res$par[1]:res$par[2]],
              hm_train <- H3K36me3_train[,res$par[1]:res$par[2]],
              hm_train <- H3K27me3_train[,res$par[1]:res$par[2]],
              hm_train <- H3K9me3_train[,res$par[1]:res$par[2]]
      ) 
      switch (res$par[3],
              hm_validation_test <- H3K4me3_validation_test[,res$par[1]:res$par[2]],
              hm_validation_test <- H3K4me1_validation_test[,res$par[1]:res$par[2]],
              hm_validation_test <- H3K36me3_validation_test[,res$par[1]:res$par[2]],
              hm_validation_test <- H3K27me3_validation_test[,res$par[1]:res$par[2]],
              hm_validation_test <- H3K9me3_validation_test[,res$par[1]:res$par[2]]
      ) 
      MP_threshold <- res$par[4]
      mp <- res$par[6:(5+res$par[5])]
      checked_positions <- 1:(ncol(hm_train)-length(mp)+1) # Niels: +1
      clusterExport(cl, c("hm_train", "hm_validation_test", "MP_threshold", "mp", "checked_positions"))
      train_df <- cbind.data.frame(train_df, parSapply(cl,rownames(train_df),function(g){
        sum(sapply(checked_positions, function(pos){cor(mp, hm_train[g,pos:(pos+length(mp)-1)])>MP_threshold}),na.rm = T)
      }))
      validation_test_df <- cbind.data.frame(validation_test_df, 
        parSapply(cl, rownames(validation_test_df),function(g){
          sum(sapply(checked_positions, function(pos){cor(mp, hm_validation_test[g,pos:(pos+length(mp)-1)])>MP_threshold}),na.rm = T)
        }))
      if(report){print(paste("Train mean squared error: ", res$value))} 
      if(res$value <= target_mse){
        improving <- FALSE
      }  
    }
    else {
      no_improvement_round <- no_improvement_round + 1
      maxit <- maxit + 1
      swarm_size <- swarm_size + 1
      maxit_stagnate <- initial_maxit + 1
      if(no_improvement_round == maxit_stagnate){
        improving <- FALSE
      }
    }  
  }
  stopCluster(cl)
  print(paste("Training finished with ", res$value, " mse. Number of trained patterns: ", ncol(train_df)-1, ". Time: ", Sys.time()-t1))
  colnames(train_df) <- c("GE",paste("Pattern", 1:(ncol(train_df)-1), sep="_"))
  colnames(validation_test_df) <- colnames(train_df)
  original_train_df <- train_df
  # mse_scores <- c()
  # auc_final <- c()
  
  # split <- Sample_Split_mult(Label = validation_test_df$GE,N_splits = k_fold - 1)
  #### K-fold crossvalidation ####
  #for(k in 0:(k_fold-1)){
    
  #### Split into validation and test df ####
  set.seed(42)
  split <- sample.split(validation_test_df$GE, SplitRatio = 0.5)
  test_df <- validation_test_df[split,]
  validation_df <- validation_test_df[!split,]

    #### Backward elimination ####
    # validation_accuracy <- round(auc(response = validation_df[,1], 
    #   predictor = predict(xgb.train(data = xgb.DMatrix(data = as.matrix(train_df[,-1]), label = train_df[,1]),
    #   nrounds = nrounds, eta = eta, nthread = Paths$n_workers, objective = "reg:squarederror", verbose = 0, 
    #   eval.metric = "auc", tree_method = "hist"), 
    #   xgb.DMatrix(data = as.matrix(validation_df[,-1]), label = validation_df[,1])), quiet = T), 
    #   rounding_decimal)
  
    predictions <- predict(xgb.train(data = xgb.DMatrix(data = as.matrix(train_df[,-1]), label = train_df[,1]),
                                     nrounds = nrounds, eta = eta, nthread = Paths$n_workers, objective = "reg:squarederror", verbose = 0, 
                                     tree_method = "hist"), 
                         xgb.DMatrix(data = as.matrix(validation_df[,-1]), label = validation_df[,1]))
    validation_mse <- mean((predictions - validation_df[,1])^2)
  
  
    pattern <- ncol(train_df)
    while(pattern != 2){
      be_train_df <- train_df[,-pattern]
      be_validation_df <- validation_df[,-pattern]
      be_test_df <- test_df[,-pattern]
      be_accuracy <- round(auc(response = be_validation_df[,1], 
        predictor = predict(xgb.train(
        data = xgb.DMatrix(data = as.matrix(be_train_df[,-1]), label = be_train_df[,1]), 
        nthread = Paths$n_workers, nrounds = nrounds, eta = eta, objective = "reg:squarederror", verbose = 0), 
        xgb.DMatrix(data = as.matrix(be_validation_df[,-1]), label = be_validation_df[,1])), quiet = T), 
        rounding_decimal)
      
      
      predictions <- predict(xgb.train(
        data = xgb.DMatrix(data = as.matrix(be_train_df[,-1]), label = be_train_df[,1]), 
        nthread = Paths$n_workers, nrounds = nrounds, eta = eta, objective = "reg:squarederror", verbose = 0), 
        xgb.DMatrix(data = as.matrix(be_validation_df[,-1]), label = be_validation_df[,1]))
      be_mse <- mean((predictions - be_validation_df[,1])^2)
      
      
      if(be_mse <= validation_mse){
        train_df <- be_train_df
        validation_df <- be_validation_df
        test_df <- be_test_df
        validation_mse <- be_mse
        pattern <- ncol(train_df)
      }
      pattern <- pattern - 1
    }
    
    #### XGB hyperparameter tuning ####
    xgb_train <- xgb.DMatrix(data = as.matrix(train_df[,-1]), label = train_df[,1])
    xgb_validation <- xgb.DMatrix(data = as.matrix(validation_df[,-1]), label = validation_df[,1])
    
    
    res <- psoptim(par = rep(NA, length(hyperparameter_lower)), fn = hyperparameter_tuning,Dataset = xgb_validation,fn_train = xgb_train,# xgb_validation substituted by Dataset , 
                   lower = hyperparameter_lower, upper = hyperparameter_upper, 
                   control = list(vectorize = T, abstol = 0.1, 
                                  trace = report, REPORT = report_frequency, 
                                  s = hp_swarm_size, w = exploitation_values, maxit = hp_maxit, c.p = c_p, c.g = c_g, 
                                  maxit.stagnate = hp_maxit_stagnate))
    
    
    #### Final test accuracy of k-fold ####
    #xgb_train <- xgb.DMatrix(data = as.matrix(train_df[,-1]), label = train_df[,1])
    #xgb_validation <- xgb.DMatrix(data = as.matrix(validation_df[,-1]), label = validation_df[,1])
    xgb_test <- xgb.DMatrix(data = as.matrix(test_df[,-1]), label = test_df[,1])
    
    XGB_model <- xgb.train(data = xgb_train, nthread = Paths$n_workers,
                           nrounds = res$par[1], eta = res$par[2], gamma = res$par[3], max_depth = round(res$par[4]), 
                           lambda = res$par[5], alpha = res$par[6], min_child_weight = res$par[7], subsample = res$par[8], 
                           objective = "reg:squarederror", tree_method = "exact",
                           #watchlist=list(train = xgb_validation, test = xgb_test), # train on validation set before application to test
                           #early_stopping_rounds = xgb_early_stopping_rounds,
                           verbose = 0)
    
    # test_accuracy <- round(auc(response = test_df[,1], predictor = predict(XGB_model, xgb.DMatrix(data = as.matrix(test_df[,-1]))), quiet = T), rounding_decimal)
    
    predictions <- predict(XGB_model,xgb.DMatrix(data = as.matrix(test_df[,-1]), label = test_df[,1]))
    
    
  #}
  
  #### Get mean and SD of AUC score for cell line ####
  test_mse <- mean((predictions - test_df[,1])^2)
  pearson_r <- cor(predictions,test_df[,1])
  cell_line_stats <- c(test_mse, pearson_r)
  
  #cell_line_stats <- c(cell_line, test_mse, pearson_r)
  print(paste("Run completed for cell line ", cell_line, ". Duration: ", Sys.time()-t1, " minutes. Test MSE: ", test_mse, "Pearson R:", pearson_r ))
    
  #### Save statistics and dataframes ####
  setwd(Paths$dataset_path)
  dir.create(cell_line)
  setwd(paste(Paths$dataset_path,"/", cell_line, sep=""))
  patterns <- patterns[-1,]
  # typecast to numeric
  patterns$Start <- floor(as.numeric(patterns$Start))
  patterns$End <- floor(as.numeric(patterns$End))
  patterns$HM <- floor(as.numeric(patterns$HM))
  patterns$Width <- floor(as.numeric(patterns$Width))
  
  ###############
  
  # # remove Patterns that are removed by BE
  patterns <- patterns[patterns$Pattern_name %in% colnames(train_df),]
  # patterns <- patterns[as.numeric(gsub("Pattern_","",colnames(train_df)[-1])),]
  # patterns$Pattern_name <- colnames(train_df)[-1]

  save(patterns, file = paste(cell_line, date, "patterns_regression.RData", sep = "_")) # Pattern characteristics for the particular cell line are stored
  #train_df <- original_train_df
  save(train_df, file = paste(cell_line, date, "train_regression_df.RData", sep = "_")) # Training set consisting of the feature matrix with all pattern frequencies before backward elimination

  # save validation set and test set seperatly
  save(validation_test_df, file = paste(cell_line, date, "validation_test_regression_df.RData", sep = "_")) # Validation and test set consisting of the feature matrix with all pattern frequencies before backward elimination
  save(validation_df, file = paste(cell_line, date, "validation_regression_df.RData", sep = "_"))
  save(test_df, file = paste(cell_line, date, "test_regression_df.RData", sep = "_"))
  
  #### save stats ####
  New_line_str <- paste(grep(cell_line,cell_lines),cell_line, test_mse, pearson_r, Sys.time()-t1, ncol(train_df)-1,sep=",")
  write(New_line_str, file = paste0(Paths$analysis_data,"Statistics/stats_regression.csv"), append = TRUE, sep = "\n") 
  
  ### save Model ####
  saveRDS(XGB_model, file = paste0(Paths$dataset_path,"/",cell_line,"/","XGB_model_regression",date,".rds"))
  saveRDS(res, file = paste0(Paths$dataset_path,"/",cell_line,"/","Parameters_regression",date,".rds"))
  
  # remove obsolete objects
  rm(list = c("XGB_model","res","predictions"))
  gc()
  
}

